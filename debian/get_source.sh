#!/bin/sh -e
set -x
git clone https://github.com/henkboom/pax-britannica.git
cd ./pax-britannica
# Should really read version number from Info.plist, but I'm too lazy to parse
# the XML dynamically...
VERSION=1.0.0
git archive --format=tar --prefix=pax-britannica-${VERSION}/ HEAD \
  -o ../pax-britannica_${VERSION}.orig.tar
gzip --best ../pax-britannica_${VERSION}.orig.tar
for sub in dokidoki dokidoki-support; do
    git submodule update --init ${sub}
    cd ./${sub}
    git archive --format=tar HEAD --prefix=${sub}/ \
      -o ../../pax-britannica_${VERSION}.orig-${sub}.tar
    gzip --best ../../pax-britannica_${VERSION}.orig-${sub}.tar
    cd ..
done
cd ..
